﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly SetPartnerPromoCodeLimitRequest _setPartnerPromoCodeLimitRequest;
        private readonly Mock<ICurrentDateTimeProvider> _currentDateTimeProvider;
        private Partner _partner;
        private IFixture fixture;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
            _setPartnerPromoCodeLimitRequest = fixture.Build<SetPartnerPromoCodeLimitRequest>().OmitAutoProperties().Create();
            _partner = fixture.Build<Partner>().OmitAutoProperties().Create();
            _currentDateTimeProvider = fixture.Freeze<Mock<ICurrentDateTimeProvider>>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ParterNotFound_Return404()
        {
            //Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _setPartnerPromoCodeLimitRequest);
            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ParterIsBlocked_Return404()
        {
            //Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = PartnerBuilder.CreateBasePartner();

            partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _setPartnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        //если лимит не закончился, то количество NumberIssuedPromoCodes обнуляется;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NumberIssuedPromoCodesEqualZeroWhenLimitIsNotOver_ReturnZero()
        {
            //Arrange
            _partner = PartnerBuilder.CreateBasePartner().WithOnlyOneActiveLimit();
            SetPartnerPromoCodeLimitRequest request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                                                             .OmitAutoProperties()
                                                             .Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partner.Id))
                .ReturnsAsync(_partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partner.Id, request);
            //Assert
            _partner.NumberIssuedPromoCodes.Should().Be(0);

        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NumberIssuedPromoCodesNotEqualZeroWhenLimitIsOver_ReturnFive()
        {
            //Arrange
            _partner = PartnerBuilder.CreateBasePartner().WithNotActiveLimit();
            SetPartnerPromoCodeLimitRequest request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                                                             .OmitAutoProperties()
                                                             .Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partner.Id))
                .ReturnsAsync(_partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partner.Id, request);

            //Assert
            _partner.NumberIssuedPromoCodes.Should().Be(5);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(0)]
        [InlineData(-5)]
        public async void SetPartnerPromoCodeLimitAsync_LimitShouldBeMoreThanZero_ReturnsBadRequest(int limit)
        {
            //Arrange
            var partnerId = Guid.Empty;
            var partner = PartnerBuilder.CreateBasePartner().WithOnlyOneActiveLimit();
            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var partnerPromoCodeLimitRequest = fixture
                .Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, limit)
                .Create();
            //Act

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimitRequest);

            //Assert
            if (limit < 0)
            {
                result.Should().BeAssignableTo<BadRequestObjectResult>();
            }

        }

        [Fact]
        public async void SetPartnerPromocodeLimitAsync_SetNewLimit_ShouldTurnOffPreviousLimit()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().WithOnlyOneActiveLimit();


            var promocodeLimitRequestDTO = fixture
                .Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, 2)
                .Create();

            var activeLimitId = partner.PartnerLimits.FirstOrDefault().Id;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, promocodeLimitRequestDTO);

            //// Assert
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => x.Id == activeLimitId);
            activeLimit.CancelDate.Should().HaveValue();
        }

        // 6. Нужно убедиться, что сохранили новый лимит в базу данных(это нужно проверить Unit-тестом);
        [Fact]
        public async void SetPartnerPromocodeLimitAsync_AddNewLimit_NewLimitShouldBeSavedToDB()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().WithOnlyOneActiveLimit();

            var partnerPromoCodeLimitRequest = fixture
                .Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, 2)
                .Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);

            //// Assert
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Once);
        }




    }
}